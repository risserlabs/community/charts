{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "sonarqube.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "sonarqube.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Calculate sonarqube certificate
*/}}
{{- define "sonarqube.sonarqube-certificate" -}}
{{- if .Values.ingress.sonarqube.tls.certificate -}}
{{- printf .Values.ingress.sonarqube.tls.certificate -}}
{{- else -}}
{{- printf "%s-gateway" (include "sonarqube.name" .) -}}
{{- end -}}
{{- end -}}

{{/*
Calculate sonarqube hostname
*/}}
{{- define "sonarqube.sonarqube-hostname" -}}
{{- if .Values.config.sonarqube.hostname -}}
{{- printf .Values.config.sonarqube.hostname -}}
{{- else -}}
{{- if .Values.ingress.sonarqube.enabled -}}
{{- printf .Values.ingress.sonarqube.hostname -}}
{{- else -}}
{{- printf "%s-release-sonarqube.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate sonarqube base url
*/}}
{{- define "sonarqube.sonarqube-base-url" -}}
{{- if .Values.config.sonarqube.baseUrl -}}
{{- printf .Values.config.sonarqube.baseUrl -}}
{{- else -}}
{{- if .Values.ingress.sonarqube.enabled -}}
{{- $hostname := ((not (include "sonarqube.sonarqube-hostname" .)) | ternary .Values.ingress.sonarqube.hostname (include "sonarqube.sonarqube-hostname" .)) -}}
{{- $protocol := (.Values.ingress.sonarqube.tls.enabled | ternary "https" "http") -}}
{{- printf "%s://%s" $protocol $hostname -}}
{{- else -}}
{{- printf "http://%s" (include "sonarqube.sonarqube-hostname" .) -}}
{{- end -}}
{{- end -}}
{{- end -}}
