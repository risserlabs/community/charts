{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "supabase.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "supabase.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate kong certificate
*/}}
{{- define "supabase.kong-certificate" }}
{{- if (not (empty .Values.ingress.certificate)) }}
{{- printf .Values.ingress.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate kong hostname
*/}}
{{- define "supabase.kong-hostname" -}}
{{- if .Values.config.kong.hostname -}}
{{- printf .Values.config.kong.hostname -}}
{{- else -}}
{{- if .Values.ingress.kong.enabled -}}
{{- printf .Values.ingress.kong.hostname -}}
{{- else -}}
{{- printf "%s-release-gateway.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Calculate supabase base url
*/}}
{{- define "supabase.kong-base-url" -}}
{{- if .Values.config.kong.baseUrl -}}
{{- printf .Values.config.kong.baseUrl -}}
{{- else -}}
{{- if .Values.ingress.kong.enabled -}}
{{- $hostname := ((not (include "supabase.kong-hostname" .)) | ternary .Values.ingress.kong.hostname (include "supabase.kong-hostname" .)) -}}
{{- $protocol := (.Values.ingress.kong.tls.enabled | ternary "https" "http") -}}
{{- printf "%s://%s" $protocol $hostname -}}
{{- else -}}
{{- printf "http://%s" (include "supabase.kong-hostname" .) -}}
{{- end -}}
{{- end -}}
{{- end -}}
