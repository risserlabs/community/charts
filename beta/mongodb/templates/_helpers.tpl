{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "mongodb.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "mongodb.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Calculate mongodb certificate
*/}}
{{- define "mongodb.mongodb-certificate" -}}
{{- if .Values.service.mongodb.tls.certificate.name -}}
{{- printf .Values.service.mongodb.tls.certificate.name -}}
{{- else -}}
{{- printf "%s-cert" (include "mongodb.name" .) -}}
{{- end -}}
{{- end -}}

{{/*
Calculate mongodb ca
*/}}
{{- define "mongodb.mongodb-ca" -}}
{{- if .Values.service.mongodb.tls.certificate.name -}}
{{- printf .Values.service.mongodb.tls.certificate.ca -}}
{{- else -}}
{{- printf "ca" -}}
{{- end -}}
{{- end -}}

{{/*
Calculate mongodb hostname
*/}}
{{- define "mongodb.mongodb-hostname" -}}
{{- if .Values.config.mongodb.hostname -}}
{{- printf .Values.config.mongodb.hostname -}}
{{- else -}}
{{- printf "%s.%s.svc.cluster.local" (include "mongodb.name" .) .Release.Namespace -}}
{{- end -}}
{{- end -}}
